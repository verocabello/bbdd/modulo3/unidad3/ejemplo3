﻿-- Modulo 3 Unidad 3

-- EJEMPLO 3

-- 1
-- importar los datos de excel. importar los datos de un libro excel y crear una tabla por cada hoja. que cada tabla tenga el mismo nombre que la hoja de calculo correspondiente

-- 2
-- funcion area triangulo. realizar una funcion que reciba como argumentos: base de un triangulo y altura de un triangulo. debe devolver el calculo del area del triangulo

DELIMITER //
CREATE OR REPLACE FUNCTION areaTriangulo(base int, altura int)
  RETURNS float
  BEGIN
   DECLARE area float;
   SET area=base*altura/2;
   RETURN area;
  END //
DELIMITER;

-- resultado con datos que tu le des
SELECT areaTriangulo(2,3);
-- resultados en una nueva columna
SELECT *,areaTriangulo(base,altura) FROM triangulos;
-- resultados en la columna area ya existente
UPDATE triangulos SET area=areaTriangulo(base,altura);
-- para dejar null en la columna area
UPDATE triangulos SET area=NULL;

SELECT * FROM triangulos;


-- 3 
-- funcion perimetro triangulo. realizar una funcion que reciba como argumentos: base de un triangulo, lado2 de un triangulo, lado3 de un triangulo. debe devolver el calculo del perimetro del triangulo

DELIMITER //
CREATE OR REPLACE FUNCTION perimetroTriangulo(base int, lado2 int, lado3 int)
  RETURNS float
  BEGIN
    DECLARE perimetro float;
    SET perimetro=base+lado2+lado3;
    RETURN perimetro;
  END //
DELIMITER;

-- resultado con datos que tu le des
SELECT perimetroTriangulo(5,2,8);
-- resultados en una nueva columna
SELECT *,perimetroTriangulo(base,lado2,lado3) FROM triangulos;
-- resultados en la columna perimetro ya existente
UPDATE triangulos SET perimetro=perimetroTriangulo(base,lado2,lado3);
-- para dejar null en la columna perimetro
UPDATE triangulos SET perimetro=NULL;

SELECT * FROM triangulos;


-- 4
-- procedimiento almacenado triangulos. realizar un procedimiento almacenado que cuando le llames como argumentos: Id1(id inicial) y Id2(id final). actualice el area y el perimetro de los triangulos 
-- (utilizando las funciones realizadas) que estén comprendidos entre los id pasados. (ACTUALIZA LA TABLA TRIANGULOS CON EL AREA Y EL PERIMETRO)

DELIMITER //
CREATE OR REPLACE PROCEDURE actualizarTriangulos(id1 int, id2 int)
  BEGIN
   UPDATE triangulos
    SET 
      area=areaTriangulo(base,altura),
      perimetro=perimetroTriangulo(base,lado2,lado3)
    WHERE id BETWEEN id1 AND id2;
  END //
DELIMITER;

CALL actualizarTriangulos(1,10);

SELECT * FROM triangulos;


-- 5
-- funcion area cuadrado. realizar una funcion que reciba como algumentos: lado de un cuadrado. devolver el calculo del area

DELIMITER //
CREATE OR REPLACE FUNCTION areaCuadrado(lado int)
  RETURNS float
  BEGIN
   DECLARE area int;
   SET area=POW(lado,2);
   RETURN area;
  END // 
DELIMITER;

-- resultado con datos que tu le des
SELECT areaCuadrado(5);
-- resultados en una nueva columna
SELECT *,areaCuadrado(lado) FROM cuadrados;
-- resultados en la columna area ya existente
UPDATE cuadrados SET area=areaCuadrado(lado);
-- para dejar null en la columna area
UPDATE cuadrados SET area=NULL;

SELECT * FROM cuadrados;


-- 6
-- funcion perimetro cuadrado. realizar una funcion que reciba como argumentos: lado de un cuadrado. debe devolver el calculo del perimetro

DELIMITER //
CREATE OR REPLACE FUNCTION perimetroCuadrado(lado int)
  RETURNS float
  BEGIN
    DECLARE perimetro int;
    SET perimetro=lado*4;
    RETURN perimetro;
  END //
DELIMITER;

-- resultado con datos que tu le des
SELECT perimetroCuadrado(5);
-- resultados en una nueva columna
SELECT *,perimetroCuadrado(lado) FROM cuadrados;
-- resultados en la columna perimetro ya existente
UPDATE cuadrados SET perimetro=perimetroCuadrado(lado);
-- para dejar null en la columna perimetro
UPDATE cuadrados SET perimetro=NULL;

SELECT * FROM cuadrados;


-- 7
-- procedimiento almacenado cuadrados. realizar un procedimiento almacenado que cuando le llames como argumentos: Id1(id inicial) y Id2(id final). actualice el area y el perimetro de los cuadrados 
-- (utilizando las funciones realizadas) que estén comprendidos entre los id pasados. (ACTUALIZA LA TABLA CUADRADOS CON EL AREA Y EL PERIMETRO)

DELIMITER //
CREATE OR REPLACE PROCEDURE actualizarCuadrados(id1 int, id2 int)
  BEGIN
    UPDATE cuadrados
      SET 
        area=areaCuadrado(lado),
        perimetro=perimetroCuadrado(lado)
      WHERE id BETWEEN id1 AND id2;
  END //
DELIMITER;

CALL actualizarCuadrados(1,5);

SELECT * FROM cuadrados;


-- 8
-- funcion area rectangulo. realizar una funcion que reciba como argumentos: lado1 y lado2. debe devolver el calculo del area del rectangulo

DELIMITER //
CREATE OR REPLACE FUNCTION areaRectangulo(lado1 int, lado2 int)
  RETURNS float
  BEGIN
    DECLARE area int;
    SET area=lado1*lado2;
    RETURN area;
  END //
DELIMITER;
  
-- resultado con datos que tu le des
SELECT areaRectangulo(5,2);
-- resultados en una nueva columna
SELECT *,areaRectangulo(lado1,lado2) FROM rectangulo;
-- resultados en la columna area ya existente
UPDATE rectangulo SET area=areaRectangulo(lado1,lado2);
-- para dejar null en la columna area
UPDATE rectangulo SET area=NULL;

SELECT * FROM rectangulo;


-- 9
-- funcion perimetro rectangulo. realizar una funcion que reciba como argumentos: lado1 y lado2. debe devolver el calculo del perimetro del rectangulo

DELIMITER //
CREATE OR REPLACE FUNCTION perimetroRectangulo(lado1 int, lado2 int)
  RETURNS float
  BEGIN
    DECLARE perimetro int;
    SET perimetro=(lado1*2)+(lado2*2);
    RETURN perimetro;
  END //
DELIMITER;

-- resultado con datos que tu le des
SELECT perimetroRectangulo(5,2);
-- resultados en una nueva columna
SELECT *,perimetroRectangulo(lado1,lado2) FROM rectangulo;
-- resultados en la columna perimetro ya existente
UPDATE rectangulo SET perimetro=perimetroRectangulo(lado1,lado2);
-- para dejar null en la columna perimetro
UPDATE rectangulo SET perimetro=NULL;


SELECT * FROM rectangulo;


-- 10
-- procedimiento almacenado rectangulo. realizar un procedimiento almacenado que cuando le llames como argumentos: Id1(id inicial) y Id2(id final). actualice el area y el perimetro de los rectangulos 
-- (utilizando las funciones realizadas) que estén comprendidos entre los id pasados. (ACTUALIZA LA TABLA RECTANGULOS CON EL AREA Y EL PERIMETRO)

DELIMITER //
CREATE OR REPLACE PROCEDURE actualizarRectangulo(id1 int, id2 int)
  BEGIN
    UPDATE rectangulo
      SET 
        area=areaRectangulo(lado1,lado2),
        perimetro=perimetroRectangulo(lado1,lado2)
      WHERE id BETWEEN id1 AND id2;
  END //
DELIMITER;

CALL actualizarRectangulo(1,5);

SELECT * FROM rectangulo;


-- 11
-- funcion area circulo. realizar una funcion que reciba como argumentos: radio. devolver el calculo del area

DELIMITER //
CREATE OR REPLACE FUNCTION areaCirculo(radio int)
  RETURNS float
  BEGIN
    DECLARE area int;
    SET area=PI()*POW(radio,2);
    RETURN area;
  END //
DELIMITER;

-- resultado con datos que tu le des
SELECT areaCirculo(5);
-- resultados en una nueva columna
SELECT *,areaCirculo(radio) FROM circulo;
-- resultados en la columna area ya existente
UPDATE circulo SET area=areaCirculo(radio);
-- para dejar null en la columna area
UPDATE circulo SET area=NULL;


SELECT * FROM circulo;


-- 12
-- funcion perimetro circulo. realizar una funcion que reciba como argumentos: radio. devolver el calculo del area

DELIMITER //
CREATE OR REPLACE FUNCTION perimetroCirculo(radio int)
  RETURNS float
  BEGIN
    DECLARE perimetro int;
    SET perimetro=2*PI()*radio;
    RETURN perimetro;
  END //
DELIMITER;

-- resultado con datos que tu le des
SELECT perimetroCirculo(5);
-- resultados en una nueva columna
SELECT *,perimetroCirculo(radio) FROM circulo;
-- resultados en la columna area ya existente
UPDATE circulo SET perimetro=perimetroCirculo(radio);
-- para dejar null en la columna area
UPDATE circulo SET perimetro=NULL;


SELECT * FROM circulo;


-- 13
-- procedimiento almacenado circulo. realizar un procedimiento almacenado que cuando le llames como argumentos: Id1(id inicial), Id2(id final) y tipo(a, b o null). actualice el area y el perimetro de los circulos 
-- (utilizando las funciones realizadas) que estén comprendidos entre los id pasados y que sea del tipo pasado. (ACTUALIZA LA TABLA CIRCULO CON EL AREA Y EL PERIMETRO)

DELIMITER //
CREATE OR REPLACE PROCEDURE actualizarCirculo(id1 int, id2 int, t char(1)
  BEGIN
    IF (t IS NULL) THEN
      UPDATE circulo
      SET 
        area=areaCirculo(radio),
        perimetro=perimetroCirculo(radio)
      WHERE id BETWEEN id1 AND id2;
    ELSE
      UPDATE circulo
      SET
        area=areaCirculo(radios),
        perimetro=perimetroCirculo(radio)
      WHERE id BETWEEN id1 AND id2 
      AND tipo=t;
    END IF;
  END //
DELIMITER;

CALL actualizarCirculo(1,5,a);

SELECT * FROM circulo;


-- 14
-- funcion media. realizar una funcion que reciba como argumentos 4 notas. devolver el calculo de la nota media

DELIMITER //
CREATE OR REPLACE FUNCTION media(nota1 int, nota2 int, nota3 int, nota4 int)
  RETURNS float
  BEGIN
    DECLARE resultado int;
    SET resultado=(nota1+nota2+nota3+nota4)/4;
    RETURN resultado;
  END //
DELIMITER;

SELECT media(5,6,8,7);


-- 15
-- funcion minimo. realizar una funcion que reciba como argumentos 4 notas. devover el calculo de la nota minima

DELIMITER //
CREATE OR REPLACE FUNCTION minima(nota1 int, nota2 int, nota3 int, nota4 int)
  RETURNS float
  BEGIN
    DECLARE resultado int;
    SET resultado=LEAST(nota1,nota2,nota3,nota4);
    RETURN resultado;
  END //
DELIMITER;

SELECT minima(5,6,8,7);

-- 16
-- funcion maximo. realizar una funcion que reciba como argumentos 4 notas. devolver el calculo de la nota maxima

DELIMITER //
CREATE OR REPLACE FUNCTION maxima(nota1 int, nota2 int, nota3 int, nota4 int)
  RETURNS float
  BEGIN
    DECLARE resultado int;
    SET resultado=GREATEST(nota1,nota2,nota3,nota4);
    RETURN resultado;
  END //
DELIMITER;

SELECT maxima(5,6,8,7);

-- 17
-- funcion moda. realizar una funcion que reciba como argumentos 4 notas. devolver la nota que mas se repite

DELIMITER //
CREATE OR REPLACE FUNCTION moda(nota1 int, nota2 int, nota3 int, nota4 int)
  RETURNS float
  BEGIN
    DECLARE resultado int;
    CREATE OR REPLACE TEMPORARY TABLE temp(
      id int AUTO_INCREMENT,
      valor int,
      PRIMARY KEY (id)
    );
    INSERT INTO temp(valor) VALUES (nota1),(nota2),(nota3),(nota4);
    SELECT valor INTO resultado FROM temp GROUP BY valor ORDER BY COUNT(*) DESC LIMIT 1;
    RETURN resultado;
  END //
DELIMITER;

SELECT moda(5,6,5,8);

-- opcion 2

DELIMITER //
CREATE OR REPLACE FUNCTION moda1(nota1 int, nota2 int, nota3 int, nota4 int)
  RETURNS float
  BEGIN
    DECLARE resultado int;
    DECLARE maximo int;
    CREATE OR REPLACE TEMPORARY TABLE temp(
      id int AUTO_INCREMENT,
      valor int,
      PRIMARY KEY (id)
    );
    INSERT INTO temp(valor) VALUES (nota1),(nota2),(nota3),(nota4);
    SELECT MAX(n) INTO maximo FROM (SELECT valor, COUNT(*) n FROM temp GROUP BY valor) c1;
      SELECT AVG(valor) INTO resultado FROM (SELECT valor, COUNT(*) n FROM temp GROUP BY valor) c1 WHERE n=maximo;
    RETURN resultado;
  END //
DELIMITER;

SELECT moda1(5,6,5,8);

-- 18
-- procedimiento almacenado alumnos. realizar un procedimiento almacenado que cuando le llames como argumentos: id1(id inicial), id2(id final). actualizar las notas minima, maxima, media y moda de los alumnos (utilizando
-- funciones realizadas) que esten comprendidos entre los id pasados. tambien debe actualizar la tabla grupos colocando la nota media de los alumnos que esten comprendidos entre los id pasados y por grupo.

DELIMITER //
CREATE OR REPLACE PROCEDURE actualizarAlumnos(id1 int, id2 int)
  BEGIN
    UPDATE alumnos
    SET 
      media=media(nota1,nota2,nota3,nota4),
      min=minima(nota1,nota2,nota3,nota4),
      max=maxima(nota1,nota2,nota3,nota4),
      moda=moda(nota1,nota2,nota3,nota4)
      WHERE id BETWEEN id1 AND id2;
    UPDATE grupos
    SET
      media=(SELECT AVG(media) FROM alumnos WHERE grupo=1 AND id BETWEEN id1 AND id2) WHERE id=1;
    UPDATE grupos
    SET
      media=(SELECT AVG(media) FROM alumnos WHERE grupo=2 AND id BETWEEN id1 AND id2) WHERE id=2;
  END //
DELIMITER;


CALL actualizarAlumnos(1,10);


SELECT * FROM alumnos;
SELECT * FROM grupos;


-- 19
-- procedimiento almacenado conversion. realizar un procedimiento almacenado que le pasas como argumento un id y te calcula la conversion de unidades de todos los registros que esten detras de ese id. si esta escrito en cm
-- calculara m, km y pulgadas. si esta escrit oen m calculara cm, km y pulgadas y asi consecutivamente.

DELIMITER //
CREATE OR REPLACE PROCEDURE actualizarConversion(id1 int)
  BEGIN
    UPDATE conversion
    SET
      m=cm/100,
      km=cm/100000,
      pulgadas=cm/0.393701
    WHERE cm IS NOT NULL AND id>id1;
    UPDATE conversion
    SET
      cm=m*100,
      km=m/1000,
      pulgadas=cm/0.393701
    WHERE m IS NOT NULL AND id>id1;
    UPDATE conversion
    SET
      cm=km*100000,
      m=km*1000,
      pulgadas=cm/0.393701
    WHERE km IS NOT NULL AND id>id1;
    UPDATE conversion
    SET
      cm=pulgadas*2.54,
      m=pulgadas*254,
      km=pulgadas*254000
    WHERE pulgadas IS NOT NULL AND id>id1;
  END //
DELIMITER;

CALL actualizarConversion(4);

SELECT * FROM conversion;